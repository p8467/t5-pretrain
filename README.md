
<!-- PROJECT LOGO -->
<div align="center">
<p>
  <a href="https://gitlab.com/prosa.ai/platform/template">
    <img src="resources/images/prosa_logo.jpeg" alt="Logo">
  </a>

  <h3 align="center">T5 Pretrain</h3>

  <p align="center">
   T5 Pretrain Repository
    <br />
    <a href="https://gitlab.com/prosa.ai/platform/template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://pm.dev.prosa.ai">Report Bug</a>
    ·
    <a href="https://pm.dev.prosa.ai">Request Feature</a>
  </p>
</p>
</div>


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Usage](#usage)
* [Acknowledgements](#acknowledgements)
* [List of Maintainers](#list-of-maintainers)
* [License](#license)



<!-- ABOUT THE PROJECT -->
## About the Project

this repository is dedicated to maintain prosa t5 indonlu pretraining project<br>

### Built With

* Transformers
* Datasets
* Flax

<!-- GETTING STARTED -->
# Getting Started

## Prerequisites
Python v3.8

## Installation
- `poetry install` or `pip install -e .` (using Poetry is recommended)
- - If using Poetry, activate `poetry shell` or prefix your python commands with `poetry [cmd]`.

## Usage
Before running the project, you must create a new .env file and put it inside the config folder. Use the provided .env.template for easier time setting up one. For more information about available settings, read the CONFIG.md.

### Running as Docker Containers

1. Build the image by running `$docker build -t <image name>:<tag> .`
2. Edit the docker-compose.yaml with the image you want to deploy.
3. Run `$docker-compose up`.


<!-- ACKNOWLEDGEMENTS -->
# Acknowledgements
This project is possible thanks to many third-party and open source libraries listed in pyproject.toml.

# List of Maintainers
* [Haniah Wafa](mailto:haniah.wafa@prosa.ai)
* [Miftahul Mahfuzh](mailto:miftahul.mahfuzh@prosa.ai)

# License
Copyright (c) 2020, [Prosa.ai](https://prosa.ai).
<!-- MARKDOWN LINKS & IMAGES -->

export CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export XLA_FLAGS=--xla_gpu_force_compilation_parallelism=1
export M=/home/miftah
export RAW=$M/core_cxbert/vocab_cxm/mix5050/mixcxm5050databert
export L=all_indonlu
export L2=test
export ID=1
export R=$M/lm-corpus
export RAW1=$R/oscar/$L/$L\_dedup_$ID.berttext
export RAW2=$R/oscar/$L/$L\_dedup.berttext
export RAW3=$R/$L/$L.txt
export RAW32=$R/$L2/$L2.txt
export RAW4=$R/wiki/$L/$L\wiki-20200501-pages-meta-current_$ID.berttext
export RAW5=$R/wiki/$L/$L\wiki.berttext
export RAW6=$R/$L/$L\_dedup_$ID.txt
export C=$R/$L/cache
export SRV=$M/huggingface
export MOD=t5
export H=lm
export D=id
export Y=24
export S=256
export V=L$Y\_S$S
export ARC=$SRV/$MOD/$MOD\_$H\_$D\_$V
python run_t5_mlm_flax.py \
  --train_file $RAW3 \
  --cache_dir $C \
  --output_dir $ARC/$L \
  --overwrite_output_dir \
  --model_type $MOD \
  --config_name $D/$MOD/L$Y \
  --tokenizer_name $D/$MOD/L$Y \
  --use_fast_tokenizer true \
  --validation_split_percentage 1 \
  --per_device_train_batch_size 32 \
  --per_device_eval_batch_size 32 \
  --max_seq_length $S \
  --learning_rate 2e-5 \
  --num_train_epochs 5 \
  --save_total_limit 2 \
  --save_steps 10000 \
  --eval_steps 10000 \
  --warmup_steps 10000 \
  --logging_steps 10000 \
  --do_train \
  --do_eval \
  --seed 42
